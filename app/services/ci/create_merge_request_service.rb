module Ci
  class CreateMergeRequestService
    def execute(project, user, params)
      attributes = params[:object_attributes]
      state = attributes[:state]
      work_in_progress = attributes[:work_in_progress]
      sha = attributes[:last_commit][:id]
      ref = attributes[:source_branch]

      unless state == 'opened' || state == 'reopened'
        return false
      end

      if work_in_progress
        return false
      end

      unless ref && sha.present?
        return false
      end

      # Skip branch removal
      if sha == Ci::Git::BLANK_SHA
        return false
      end

      tag = ref.start_with?('refs/tags/')
      commit = project.gl_project.ensure_ci_commit(sha)

      unless commit.skip_ci?
        commit.update_committed!
        commit.create_builds(ref, tag, user)
      end

      commit
    end
  end
end
